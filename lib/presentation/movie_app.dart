import 'package:fluro/fluro.dart';
import 'package:movie_recomendation/core/di/service_locator.dart';
import 'package:movie_recomendation/data/db/database_helper.dart';

import 'app_routes.dart';

class MovieRecommendApp {

  //add more global object here if needed
  DatabaseHelper _db;

  Future<void> onCreate() async {
    _db = sl.get<DatabaseHelper>();
  }

  Future<void> onTerminate() async {
    await _db.close();
  }

  void configureRoute() {
    AppRoutes.configureRoutes(sl.get<Router>());
  }

}