import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:movie_recomendation/env.dart';
import 'package:movie_recomendation/core/di/service_locator.dart';
import 'package:movie_recomendation/core/util/log/log.dart';
import 'package:movie_recomendation/generated/i18n.dart';
import 'package:movie_recomendation/presentation/screen/home/home_page.dart';
import 'package:movie_recomendation/presentation/screen/home/bloc/home_bloc.dart';

import 'movie_app.dart';
import 'movie_provider.dart';

class MovieBaseWidget extends StatefulWidget {

  final MovieRecommendApp _application;

  MovieBaseWidget(this._application);

  @override
  State createState() {
    return MovieBaseWidgetState(_application);
  }
}

class MovieBaseWidgetState extends State<MovieBaseWidget> {

  MovieBaseWidgetState(this._application);

  final MovieRecommendApp _application;

  @override
  void dispose()async{
    print('dispose');
    await _application.onTerminate();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final app = MaterialApp(
        title: Env.value.appName,
        localizationsDelegates: [
          S.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate
        ],
        supportedLocales: S.delegate.supportedLocales,
        debugShowCheckedModeBanner: true,
        theme: new ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: BlocProvider<HomeBloc>(
          create: (context) => sl(),
          child: HomePage(),
        ),
//        home: HomePage(),
        onGenerateRoute: sl.get<Router>().generator,
    );
    print('initial core.route = ${app.initialRoute}');

    final appProvider = MovieProvider(child: app, application: _application);
    return appProvider;
  }
}