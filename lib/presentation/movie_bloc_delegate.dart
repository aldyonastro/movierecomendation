import 'package:flutter_bloc/flutter_bloc.dart';

class MovieBlocDelegate extends BlocObserver {

  @override
  void onEvent(Bloc bloc, Object event) {
    super.onEvent(bloc, event);
    print("Event Delegate --> $bloc, event: $event ");
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    super.onTransition(bloc, transition);
    print("Transition Delegate --->: $bloc, transition: $transition ");
  }

  @override
  void onError(Cubit cubit, Object error, StackTrace stacktrace) {
    super.onError(cubit, error, stacktrace);
    print("Error Delegate --->: $cubit, transition: $error");
  }

}