import 'package:flutter/material.dart';

import 'movie_app.dart';


class MovieProvider extends InheritedWidget {

  final MovieRecommendApp application;

  MovieProvider({Key key, Widget child, this.application})
      : super(key: key, child: child);

  bool updateShouldNotify(_) => true;

  static MovieProvider of(BuildContext context) {
    return context.inheritFromWidgetOfExactType(MovieProvider) as MovieProvider;
  }

}
