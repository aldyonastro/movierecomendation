import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:movie_recomendation/data/model/movie_list_response.dart';
import 'package:movie_recomendation/domain/entities/movie.dart';


abstract class HomeState extends Equatable {
  const HomeState();

  @override
  List<Object> get props => [];
}

class HomeUninitialized extends HomeState {

}

class HomeRefreshed extends HomeState {

}

class HomeError extends HomeState {

  final String errorMessage;

  const HomeError({@required this.errorMessage});

  @override
  String toString() => 'HomeError';

  @override
  List<Object> get props => [errorMessage];
}

class HomeLoaded extends HomeState {
  final List<Movie> movieList;
  final bool hasReachedMax;
  final String keyword;

  const HomeLoaded({@required this.movieList, @required this.hasReachedMax, this.keyword});

  HomeLoaded copyWith({ MovieListResponse movieList, bool hasReachedMax, }) {
    return HomeLoaded(
      movieList: movieList ?? this.movieList,
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
    );
  }

  @override
  String toString() {
    return 'PostLoaded { posts: ${movieList.length}, hasReachedMax: $hasReachedMax }';
  }

  @override
  List<Object> get props => [movieList, hasReachedMax];
}