import 'dart:async';
import 'dart:math';

import 'package:bloc/bloc.dart';
import 'package:english_words/english_words.dart';
import 'package:injectable/injectable.dart';
import 'package:movie_recomendation/core/error/exception.dart';
import 'package:movie_recomendation/core/error/failure.dart';
import 'package:movie_recomendation/core/util/log/log.dart';
import 'package:movie_recomendation/domain/entities/search_result.dart';
import 'package:movie_recomendation/domain/get_movie_search_interactor.dart';

import 'home_event.dart';
import 'home_state.dart';

@injectable
class HomeBloc extends Bloc<HomeEvent, HomeState>{
  final GetMovieSearchInteractor _getMovieUseCase;

  int pageIndex = 1;
  String wordOfTheDay = "";
  Log _log;


  HomeBloc(this._getMovieUseCase, this._log): super(HomeUninitialized()){
    wordOfTheDay = all.elementAt(Random().nextInt(all.length));
    _log.info("Search title >>> "+wordOfTheDay);
  }

  @override
  Stream<HomeState> mapEventToState(HomeEvent event) async* {
    final currentState = state;

    if(event is HomeRefresh){
      yield HomeUninitialized();
    }

    if (event is FetchMovie && !_hasReachedMax(currentState)) {
      try {
        if (currentState is HomeUninitialized) {
          String searchString = event.searchKeyword ?? wordOfTheDay;
          final result = await _fetchMovie(searchString, pageIndex);
          yield HomeLoaded(movieList: result.searchResult, hasReachedMax: false, keyword: searchString);
        }

        if (currentState is HomeLoaded) {
          pageIndex++;
          final result = await _fetchMovie(event.searchKeyword, pageIndex);

          int pageCount = (int.parse(result.totalResult) / 10).ceil();

          yield pageIndex >= pageCount ?
            currentState.copyWith(hasReachedMax: true) :
            HomeLoaded( movieList: currentState.movieList + result.searchResult, hasReachedMax: false);
        }


      } catch (_) {
        if(_ is MovieNotFoundException) {
          yield HomeError(errorMessage: "Movie not found");
        } else {
          yield HomeError(errorMessage: _.toString());
        }
      }
    }
  }

  bool _hasReachedMax(HomeState state) =>
      state is HomeLoaded && state.hasReachedMax;

  Future<SearchResult> _fetchMovie(String searchKeyword, int page) async {
    final either = await _getMovieUseCase(SearchParams(searchKeyword, page));
    return either.fold(
     (l) => throw _getFailureAndThrowExpection(l),
     (r) => r,
    );
  }

  @override
  void onTransition(Transition<HomeEvent, HomeState> transition) {
    super.onTransition(transition);
    print("Home Transition: $transition");
  }

  Exception _getFailureAndThrowExpection(Failure l) {
    if (l is ServerFailure) {
      return ServerException();
    } else if (l is CacheFailure) {
      return CacheException();
    } else {
      return UnknownException();
    }
  }
}
