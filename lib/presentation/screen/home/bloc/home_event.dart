import 'package:equatable/equatable.dart';

abstract class HomeEvent extends Equatable {
  HomeEvent();
  List<Object> get props => [];
}

class FetchMovie extends HomeEvent {
  String searchKeyword;

  FetchMovie({this.searchKeyword});

  @override
  List<Object> get props => [searchKeyword];
}

class HomeRefresh extends HomeEvent {

}


