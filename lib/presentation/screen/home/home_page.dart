import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_recomendation/presentation/screen/home/widget/movie_list_item_card.dart';
import 'package:movie_recomendation/presentation/widget/bottom_loader.dart';
import 'package:progress_indicators/progress_indicators.dart';

import 'bloc/home_bloc.dart';
import 'bloc/home_event.dart';
import 'bloc/home_state.dart';

class HomePage extends StatefulWidget {
  static const String PATH = '/';

  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();

}

class _HomePageState extends State<HomePage> {

  final _scrollController = ScrollController();
  final _scrollThreshold = 200.0;
  HomeBloc _homeBloc;

  var _keywordController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(_onScroll);
    _homeBloc = BlocProvider.of<HomeBloc>(context);
    _homeBloc.add(FetchMovie());
  }

  @override
  void dispose() {
    super.dispose();
    _keywordController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Movie Recomendation"),
      ),
      body: buildFeedList(context),
    );
  }

  void _onScroll() {
    final maxScroll = _scrollController.position.maxScrollExtent;
    final currentScroll = _scrollController.position.pixels;
    if (maxScroll - currentScroll <= _scrollThreshold) {
      _homeBloc.add(FetchMovie(searchKeyword: _keywordController.text));
    }
  }

  Future<Null> _onRefresh() async {
    _homeBloc.add(HomeRefresh());
    _homeBloc.add(FetchMovie(searchKeyword: _keywordController.text));
    return null;
  }

  Widget buildFeedList(BuildContext ctx) {
    return BlocBuilder<HomeBloc, HomeState>(
      builder: (ctx, state) {
        if (state is HomeUninitialized) {
          return Container(
            alignment: Alignment.center,
            child: JumpingText('Fetch Data...'),
          );
        }

        if (state is HomeError) {
          HomeError he = state;
          return Center(child: Text(he.errorMessage),);
        }

        if (state is HomeLoaded)
        {
          _keywordController.text = state.keyword;

          if (state.movieList.isEmpty){
            return Center(child: Text('no movie for today'),);
          }

          return RefreshIndicator(
            onRefresh: _onRefresh,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      Expanded(
                        flex: 8,
                        child: TextField(
                          controller: _keywordController,
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.symmetric(horizontal: 16),
                              labelText: "Keyword",
                              border: OutlineInputBorder(borderSide: BorderSide(), borderRadius: BorderRadius.circular(16)),
                              hintText: 'Enter a search here',
                          ),
                        ),
                      ),
                      SizedBox(width: 16,),
                      RaisedButton(
                        onPressed: (){
                          _homeBloc.add(HomeRefresh());
                          _homeBloc.add(FetchMovie(searchKeyword: _keywordController.text));
                        },
                        color: Colors.yellow,
                        child: Text('Search'),
                      )
                    ],
                  ),
                ),
                Expanded(
                  child: ListView.builder(
                      controller: _scrollController,
                      scrollDirection: Axis.vertical,
                      itemCount: state.hasReachedMax ? state.movieList.length : state.movieList.length + 1,
                      itemBuilder: (context, index) {
                        return index >= state.movieList.length ?
                        BottomLoader() :
                        MovieListItemCard(state.movieList[index]);
                      }
                  ),
                ),
              ],
            ),
          );
        }


        return Center(
          child: CircularProgressIndicator(),
        );
      },
    );
  }

}