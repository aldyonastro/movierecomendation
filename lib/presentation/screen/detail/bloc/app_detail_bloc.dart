import 'dart:async';

import 'package:rxdart/rxdart.dart';
import 'package:rxdart/subjects.dart';

class AppDetailBloc{
  CompositeSubscription _compositeSubscription = CompositeSubscription();

  final _isShowLoading = BehaviorSubject<bool>();
//  final _appContent = BehaviorSubject<AppContent>();

  Stream<bool> get isShowLoading => _isShowLoading.stream;
//  Stream<AppContent> get appContent => _appContent.stream;


  AppDetailBloc();

  void dispose() {
    _compositeSubscription.clear();
    _isShowLoading.close();
//    _appContent.close();
  }

  void loadDetail(String appId){
    _isShowLoading.add(true);
//    StreamSubscription subscription = sl.get<AppStoreAPIRepository>().getAppDetail(appId)
//        .listen((AppContent appContent){
//      _appContent.add(appContent);
//      _isShowLoading.add(false);
//    });
//    _compositeSubscription.add(subscription);
  }

}