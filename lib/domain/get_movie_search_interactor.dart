import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';
import 'package:injectable/injectable.dart';
import 'package:movie_recomendation/core/error/exception.dart';
import 'package:movie_recomendation/core/error/failure.dart';
import 'package:movie_recomendation/core/usecase/use_case.dart';
import 'package:movie_recomendation/core/util/log/log.dart';
import 'package:movie_recomendation/domain/entities/search_result.dart';
import 'package:movie_recomendation/domain/repository/movie_repository.dart';

import 'entities/movie.dart';

@lazySingleton
class GetMovieSearchInteractor extends UseCase<SearchResult, SearchParams>{

  GetMovieSearchInteractor(this.repository);

  final IMovieRepository repository;

  @override
  Future<Either<Failure, SearchResult>> call(SearchParams params) {
    return repository.getMovieSearch(params.searchQuery, params.page);
  }

}

class SearchParams {
  SearchParams(this.searchQuery, this.page);

  final String searchQuery;
  final int page;
}