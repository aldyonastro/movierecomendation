import 'package:dartz/dartz.dart';
import 'package:movie_recomendation/core/error/exception.dart';
import 'package:movie_recomendation/core/error/failure.dart';
import 'package:movie_recomendation/domain/entities/movie.dart';
import 'package:movie_recomendation/domain/entities/search_result.dart';

// ignore: one_member_abstracts
abstract class IMovieRepository {
  Future<Either<Failure, SearchResult>> getMovieSearch(String title, int page);
}