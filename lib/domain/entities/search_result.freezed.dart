// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'search_result.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$SearchResultTearOff {
  const _$SearchResultTearOff();

// ignore: unused_element
  _SearchResult call(
      {@required String totalResult, @required List<Movie> searchResult}) {
    return _SearchResult(
      totalResult: totalResult,
      searchResult: searchResult,
    );
  }
}

// ignore: unused_element
const $SearchResult = _$SearchResultTearOff();

mixin _$SearchResult {
  String get totalResult;
  List<Movie> get searchResult;

  $SearchResultCopyWith<SearchResult> get copyWith;
}

abstract class $SearchResultCopyWith<$Res> {
  factory $SearchResultCopyWith(
          SearchResult value, $Res Function(SearchResult) then) =
      _$SearchResultCopyWithImpl<$Res>;
  $Res call({String totalResult, List<Movie> searchResult});
}

class _$SearchResultCopyWithImpl<$Res> implements $SearchResultCopyWith<$Res> {
  _$SearchResultCopyWithImpl(this._value, this._then);

  final SearchResult _value;
  // ignore: unused_field
  final $Res Function(SearchResult) _then;

  @override
  $Res call({
    Object totalResult = freezed,
    Object searchResult = freezed,
  }) {
    return _then(_value.copyWith(
      totalResult:
          totalResult == freezed ? _value.totalResult : totalResult as String,
      searchResult: searchResult == freezed
          ? _value.searchResult
          : searchResult as List<Movie>,
    ));
  }
}

abstract class _$SearchResultCopyWith<$Res>
    implements $SearchResultCopyWith<$Res> {
  factory _$SearchResultCopyWith(
          _SearchResult value, $Res Function(_SearchResult) then) =
      __$SearchResultCopyWithImpl<$Res>;
  @override
  $Res call({String totalResult, List<Movie> searchResult});
}

class __$SearchResultCopyWithImpl<$Res> extends _$SearchResultCopyWithImpl<$Res>
    implements _$SearchResultCopyWith<$Res> {
  __$SearchResultCopyWithImpl(
      _SearchResult _value, $Res Function(_SearchResult) _then)
      : super(_value, (v) => _then(v as _SearchResult));

  @override
  _SearchResult get _value => super._value as _SearchResult;

  @override
  $Res call({
    Object totalResult = freezed,
    Object searchResult = freezed,
  }) {
    return _then(_SearchResult(
      totalResult:
          totalResult == freezed ? _value.totalResult : totalResult as String,
      searchResult: searchResult == freezed
          ? _value.searchResult
          : searchResult as List<Movie>,
    ));
  }
}

class _$_SearchResult implements _SearchResult {
  const _$_SearchResult(
      {@required this.totalResult, @required this.searchResult})
      : assert(totalResult != null),
        assert(searchResult != null);

  @override
  final String totalResult;
  @override
  final List<Movie> searchResult;

  @override
  String toString() {
    return 'SearchResult(totalResult: $totalResult, searchResult: $searchResult)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _SearchResult &&
            (identical(other.totalResult, totalResult) ||
                const DeepCollectionEquality()
                    .equals(other.totalResult, totalResult)) &&
            (identical(other.searchResult, searchResult) ||
                const DeepCollectionEquality()
                    .equals(other.searchResult, searchResult)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(totalResult) ^
      const DeepCollectionEquality().hash(searchResult);

  @override
  _$SearchResultCopyWith<_SearchResult> get copyWith =>
      __$SearchResultCopyWithImpl<_SearchResult>(this, _$identity);
}

abstract class _SearchResult implements SearchResult {
  const factory _SearchResult(
      {@required String totalResult,
      @required List<Movie> searchResult}) = _$_SearchResult;

  @override
  String get totalResult;
  @override
  List<Movie> get searchResult;
  @override
  _$SearchResultCopyWith<_SearchResult> get copyWith;
}
