import 'package:freezed_annotation/freezed_annotation.dart';

import 'movie.dart';

part 'search_result.freezed.dart';

@freezed
abstract class SearchResult with _$SearchResult{
  const factory SearchResult({
    @required String totalResult,
    @required List<Movie> searchResult,
  }) = _SearchResult;
}