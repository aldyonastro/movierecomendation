// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'movie.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$MovieTearOff {
  const _$MovieTearOff();

// ignore: unused_element
  _Movie call(
      {@required String title,
      @required String year,
      @required String type,
      @required String poster}) {
    return _Movie(
      title: title,
      year: year,
      type: type,
      poster: poster,
    );
  }
}

// ignore: unused_element
const $Movie = _$MovieTearOff();

mixin _$Movie {
  String get title;
  String get year;
  String get type;
  String get poster;

  $MovieCopyWith<Movie> get copyWith;
}

abstract class $MovieCopyWith<$Res> {
  factory $MovieCopyWith(Movie value, $Res Function(Movie) then) =
      _$MovieCopyWithImpl<$Res>;
  $Res call({String title, String year, String type, String poster});
}

class _$MovieCopyWithImpl<$Res> implements $MovieCopyWith<$Res> {
  _$MovieCopyWithImpl(this._value, this._then);

  final Movie _value;
  // ignore: unused_field
  final $Res Function(Movie) _then;

  @override
  $Res call({
    Object title = freezed,
    Object year = freezed,
    Object type = freezed,
    Object poster = freezed,
  }) {
    return _then(_value.copyWith(
      title: title == freezed ? _value.title : title as String,
      year: year == freezed ? _value.year : year as String,
      type: type == freezed ? _value.type : type as String,
      poster: poster == freezed ? _value.poster : poster as String,
    ));
  }
}

abstract class _$MovieCopyWith<$Res> implements $MovieCopyWith<$Res> {
  factory _$MovieCopyWith(_Movie value, $Res Function(_Movie) then) =
      __$MovieCopyWithImpl<$Res>;
  @override
  $Res call({String title, String year, String type, String poster});
}

class __$MovieCopyWithImpl<$Res> extends _$MovieCopyWithImpl<$Res>
    implements _$MovieCopyWith<$Res> {
  __$MovieCopyWithImpl(_Movie _value, $Res Function(_Movie) _then)
      : super(_value, (v) => _then(v as _Movie));

  @override
  _Movie get _value => super._value as _Movie;

  @override
  $Res call({
    Object title = freezed,
    Object year = freezed,
    Object type = freezed,
    Object poster = freezed,
  }) {
    return _then(_Movie(
      title: title == freezed ? _value.title : title as String,
      year: year == freezed ? _value.year : year as String,
      type: type == freezed ? _value.type : type as String,
      poster: poster == freezed ? _value.poster : poster as String,
    ));
  }
}

class _$_Movie implements _Movie {
  const _$_Movie(
      {@required this.title,
      @required this.year,
      @required this.type,
      @required this.poster})
      : assert(title != null),
        assert(year != null),
        assert(type != null),
        assert(poster != null);

  @override
  final String title;
  @override
  final String year;
  @override
  final String type;
  @override
  final String poster;

  @override
  String toString() {
    return 'Movie(title: $title, year: $year, type: $type, poster: $poster)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _Movie &&
            (identical(other.title, title) ||
                const DeepCollectionEquality().equals(other.title, title)) &&
            (identical(other.year, year) ||
                const DeepCollectionEquality().equals(other.year, year)) &&
            (identical(other.type, type) ||
                const DeepCollectionEquality().equals(other.type, type)) &&
            (identical(other.poster, poster) ||
                const DeepCollectionEquality().equals(other.poster, poster)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(title) ^
      const DeepCollectionEquality().hash(year) ^
      const DeepCollectionEquality().hash(type) ^
      const DeepCollectionEquality().hash(poster);

  @override
  _$MovieCopyWith<_Movie> get copyWith =>
      __$MovieCopyWithImpl<_Movie>(this, _$identity);
}

abstract class _Movie implements Movie {
  const factory _Movie(
      {@required String title,
      @required String year,
      @required String type,
      @required String poster}) = _$_Movie;

  @override
  String get title;
  @override
  String get year;
  @override
  String get type;
  @override
  String get poster;
  @override
  _$MovieCopyWith<_Movie> get copyWith;
}
