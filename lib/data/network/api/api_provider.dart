import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:movie_recomendation/core/util/log/log.dart';
import 'package:movie_recomendation/env.dart';
import 'package:movie_recomendation/core/error/Exception.dart';
import 'package:movie_recomendation/core/util/log/dio_logger.dart';
import 'package:movie_recomendation/data/model/movie_list_response.dart';

@lazySingleton
class ApiProvider{
  static const String TAG = 'APIProvider';

  static const String _baseUrl = "http://www.omdbapi.com/";

  Dio _dio;
  DioLogger _dioLogger;

  ApiProvider(this._dioLogger){
    BaseOptions dioOptions = BaseOptions()
      ..baseUrl = ApiProvider._baseUrl;

    _dio = Dio(dioOptions);

    if(EnvType.STAGING == Env.value.environmentType){
      _dio.options.receiveTimeout = 15000;
      _dio.interceptors.add(InterceptorsWrapper(

          onRequest:(RequestOptions options) async{
            _dioLogger.onSend(TAG, options);
            return options;
          },
          onResponse: (Response response){
            _dioLogger.onSuccess(TAG, response);
            return response;
          },
          onError: (DioError error){
            _dioLogger.onError(TAG, error);
            return error;
          }
      ));
    }
  }

  Future<MovieListResponse> getMovieList(String querySearch, int page) async{
    Response response = await _dio.get("", queryParameters: {
      "apikey": "544176cb",
      "s": querySearch,
      "page": page.toString()
    });

    throwIfNoSuccess(response);
    return MovieListResponse.fromJson(response.data);
  }


  void throwIfNoSuccess(Response response) {

    if(response.data["Response"] == "False") {
      throw MovieNotFoundException();
    }

    if(response.statusCode < 200 || response.statusCode > 299) {
      throw HttpException(response);
    }
  }

}