import 'dart:async';

import 'package:dartz/dartz.dart';
import 'package:injectable/injectable.dart';
import 'package:movie_recomendation/core/error/exception.dart';
import 'package:movie_recomendation/core/error/failure.dart';
import 'package:movie_recomendation/core/util/log/log.dart';
import 'package:movie_recomendation/core/util/networks/network_utils.dart';
import 'package:movie_recomendation/data/db/movie_db_repository.dart';
import 'package:movie_recomendation/data/model/movie_list_response.dart';
import 'package:movie_recomendation/data/network/api/api_provider.dart';
import 'package:movie_recomendation/domain/entities/movie.dart';
import 'package:movie_recomendation/domain/entities/search_result.dart';
import 'package:movie_recomendation/domain/repository/movie_repository.dart';
import 'package:rxdart/rxdart.dart';

// ignore: unused_field
//@lazySingleton
@Injectable(as: IMovieRepository)
class MovieApiRepository implements IMovieRepository{

  MovieApiRepository(this._networkUtils, this._apiProvider);

  ApiProvider _apiProvider;
  MovieDbRepository _dbAppStoreRepository;
  INetworkUtils _networkUtils;

  MovieApiRepository.fromApiRepository(INetworkUtils networkUtils, MovieApiRepository apiRepository, MovieDbRepository dbRepository) {
    _networkUtils = networkUtils;
    _apiProvider = apiRepository._apiProvider;
    _dbAppStoreRepository = dbRepository;
  }

  @override Future<Either<Failure, SearchResult>> getMovieSearch(String title, int page) async {
    if (await _networkUtils.isConnected) {
      final model = await _apiProvider.getMovieList(title, page);
      final searchResultEntity = model.toEntity();
      return Right(searchResultEntity);
    } else {
      return Left(ServerFailure());
    }

  }

}