import 'package:movie_recomendation/core/util/log/log.dart';
import 'package:sqflite/sqflite.dart';

import 'database_helper.dart';

class AppDatabaseMigrationListener implements DatabaseMigrationListener{

  static const int VERSION_1_0_0 = 1;

  AppDatabaseMigrationListener(Log log);

  Log log;

  @override
  void onCreate(Database db, int version) async {

    log.info('onCreate version : $version');
      await _createDatabase(db, version);
  }

  @override
  void onUpgrade(Database db, int oldVersion, int newVersion) {
    log.info('oldVersion : $oldVersion');
    log.info('newVersion : $newVersion');
  }

  Future<void> _createDatabase(Database db, int version) async{
    if(VERSION_1_0_0 == version){
      await db.execute('CREATE TABLE MovieCache ('
          'movieTitle TEXT, '
          'description TEXT, '
          'meta TEXT)');
    }
  }
}