// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'movie_list_response.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
MovieListResponse _$MovieListResponseFromJson(Map<String, dynamic> json) {
  return _MovieListResponse.fromJson(json);
}

class _$MovieListResponseTearOff {
  const _$MovieListResponseTearOff();

// ignore: unused_element
  _MovieListResponse call(
      {@JsonKey(name: 'Search') List<MovieResponse> movies,
      @JsonKey(name: 'totalResults') String totalResults,
      @JsonKey(name: 'Response') String response}) {
    return _MovieListResponse(
      movies: movies,
      totalResults: totalResults,
      response: response,
    );
  }
}

// ignore: unused_element
const $MovieListResponse = _$MovieListResponseTearOff();

mixin _$MovieListResponse {
  @JsonKey(name: 'Search')
  List<MovieResponse> get movies;
  @JsonKey(name: 'totalResults')
  String get totalResults;
  @JsonKey(name: 'Response')
  String get response;

  Map<String, dynamic> toJson();
  $MovieListResponseCopyWith<MovieListResponse> get copyWith;
}

abstract class $MovieListResponseCopyWith<$Res> {
  factory $MovieListResponseCopyWith(
          MovieListResponse value, $Res Function(MovieListResponse) then) =
      _$MovieListResponseCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'Search') List<MovieResponse> movies,
      @JsonKey(name: 'totalResults') String totalResults,
      @JsonKey(name: 'Response') String response});
}

class _$MovieListResponseCopyWithImpl<$Res>
    implements $MovieListResponseCopyWith<$Res> {
  _$MovieListResponseCopyWithImpl(this._value, this._then);

  final MovieListResponse _value;
  // ignore: unused_field
  final $Res Function(MovieListResponse) _then;

  @override
  $Res call({
    Object movies = freezed,
    Object totalResults = freezed,
    Object response = freezed,
  }) {
    return _then(_value.copyWith(
      movies: movies == freezed ? _value.movies : movies as List<MovieResponse>,
      totalResults: totalResults == freezed
          ? _value.totalResults
          : totalResults as String,
      response: response == freezed ? _value.response : response as String,
    ));
  }
}

abstract class _$MovieListResponseCopyWith<$Res>
    implements $MovieListResponseCopyWith<$Res> {
  factory _$MovieListResponseCopyWith(
          _MovieListResponse value, $Res Function(_MovieListResponse) then) =
      __$MovieListResponseCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'Search') List<MovieResponse> movies,
      @JsonKey(name: 'totalResults') String totalResults,
      @JsonKey(name: 'Response') String response});
}

class __$MovieListResponseCopyWithImpl<$Res>
    extends _$MovieListResponseCopyWithImpl<$Res>
    implements _$MovieListResponseCopyWith<$Res> {
  __$MovieListResponseCopyWithImpl(
      _MovieListResponse _value, $Res Function(_MovieListResponse) _then)
      : super(_value, (v) => _then(v as _MovieListResponse));

  @override
  _MovieListResponse get _value => super._value as _MovieListResponse;

  @override
  $Res call({
    Object movies = freezed,
    Object totalResults = freezed,
    Object response = freezed,
  }) {
    return _then(_MovieListResponse(
      movies: movies == freezed ? _value.movies : movies as List<MovieResponse>,
      totalResults: totalResults == freezed
          ? _value.totalResults
          : totalResults as String,
      response: response == freezed ? _value.response : response as String,
    ));
  }
}

@JsonSerializable(explicitToJson: true)
class _$_MovieListResponse implements _MovieListResponse {
  const _$_MovieListResponse(
      {@JsonKey(name: 'Search') this.movies,
      @JsonKey(name: 'totalResults') this.totalResults,
      @JsonKey(name: 'Response') this.response});

  factory _$_MovieListResponse.fromJson(Map<String, dynamic> json) =>
      _$_$_MovieListResponseFromJson(json);

  @override
  @JsonKey(name: 'Search')
  final List<MovieResponse> movies;
  @override
  @JsonKey(name: 'totalResults')
  final String totalResults;
  @override
  @JsonKey(name: 'Response')
  final String response;

  @override
  String toString() {
    return 'MovieListResponse(movies: $movies, totalResults: $totalResults, response: $response)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _MovieListResponse &&
            (identical(other.movies, movies) ||
                const DeepCollectionEquality().equals(other.movies, movies)) &&
            (identical(other.totalResults, totalResults) ||
                const DeepCollectionEquality()
                    .equals(other.totalResults, totalResults)) &&
            (identical(other.response, response) ||
                const DeepCollectionEquality()
                    .equals(other.response, response)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(movies) ^
      const DeepCollectionEquality().hash(totalResults) ^
      const DeepCollectionEquality().hash(response);

  @override
  _$MovieListResponseCopyWith<_MovieListResponse> get copyWith =>
      __$MovieListResponseCopyWithImpl<_MovieListResponse>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_MovieListResponseToJson(this);
  }
}

abstract class _MovieListResponse implements MovieListResponse {
  const factory _MovieListResponse(
      {@JsonKey(name: 'Search') List<MovieResponse> movies,
      @JsonKey(name: 'totalResults') String totalResults,
      @JsonKey(name: 'Response') String response}) = _$_MovieListResponse;

  factory _MovieListResponse.fromJson(Map<String, dynamic> json) =
      _$_MovieListResponse.fromJson;

  @override
  @JsonKey(name: 'Search')
  List<MovieResponse> get movies;
  @override
  @JsonKey(name: 'totalResults')
  String get totalResults;
  @override
  @JsonKey(name: 'Response')
  String get response;
  @override
  _$MovieListResponseCopyWith<_MovieListResponse> get copyWith;
}

MovieResponse _$MovieResponseFromJson(Map<String, dynamic> json) {
  return _MovieResponse.fromJson(json);
}

class _$MovieResponseTearOff {
  const _$MovieResponseTearOff();

// ignore: unused_element
  _MovieResponse call(
      {@JsonKey(name: 'Title') String title,
      @JsonKey(name: 'Year') String year,
      @JsonKey(name: 'imdbId') String imdbId,
      @JsonKey(name: 'Type') String type,
      @JsonKey(name: 'Poster') String poster}) {
    return _MovieResponse(
      title: title,
      year: year,
      imdbId: imdbId,
      type: type,
      poster: poster,
    );
  }
}

// ignore: unused_element
const $MovieResponse = _$MovieResponseTearOff();

mixin _$MovieResponse {
  @JsonKey(name: 'Title')
  String get title;
  @JsonKey(name: 'Year')
  String get year;
  @JsonKey(name: 'imdbId')
  String get imdbId;
  @JsonKey(name: 'Type')
  String get type;
  @JsonKey(name: 'Poster')
  String get poster;

  Map<String, dynamic> toJson();
  $MovieResponseCopyWith<MovieResponse> get copyWith;
}

abstract class $MovieResponseCopyWith<$Res> {
  factory $MovieResponseCopyWith(
          MovieResponse value, $Res Function(MovieResponse) then) =
      _$MovieResponseCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'Title') String title,
      @JsonKey(name: 'Year') String year,
      @JsonKey(name: 'imdbId') String imdbId,
      @JsonKey(name: 'Type') String type,
      @JsonKey(name: 'Poster') String poster});
}

class _$MovieResponseCopyWithImpl<$Res>
    implements $MovieResponseCopyWith<$Res> {
  _$MovieResponseCopyWithImpl(this._value, this._then);

  final MovieResponse _value;
  // ignore: unused_field
  final $Res Function(MovieResponse) _then;

  @override
  $Res call({
    Object title = freezed,
    Object year = freezed,
    Object imdbId = freezed,
    Object type = freezed,
    Object poster = freezed,
  }) {
    return _then(_value.copyWith(
      title: title == freezed ? _value.title : title as String,
      year: year == freezed ? _value.year : year as String,
      imdbId: imdbId == freezed ? _value.imdbId : imdbId as String,
      type: type == freezed ? _value.type : type as String,
      poster: poster == freezed ? _value.poster : poster as String,
    ));
  }
}

abstract class _$MovieResponseCopyWith<$Res>
    implements $MovieResponseCopyWith<$Res> {
  factory _$MovieResponseCopyWith(
          _MovieResponse value, $Res Function(_MovieResponse) then) =
      __$MovieResponseCopyWithImpl<$Res>;
  @override
  $Res call(
      {@JsonKey(name: 'Title') String title,
      @JsonKey(name: 'Year') String year,
      @JsonKey(name: 'imdbId') String imdbId,
      @JsonKey(name: 'Type') String type,
      @JsonKey(name: 'Poster') String poster});
}

class __$MovieResponseCopyWithImpl<$Res>
    extends _$MovieResponseCopyWithImpl<$Res>
    implements _$MovieResponseCopyWith<$Res> {
  __$MovieResponseCopyWithImpl(
      _MovieResponse _value, $Res Function(_MovieResponse) _then)
      : super(_value, (v) => _then(v as _MovieResponse));

  @override
  _MovieResponse get _value => super._value as _MovieResponse;

  @override
  $Res call({
    Object title = freezed,
    Object year = freezed,
    Object imdbId = freezed,
    Object type = freezed,
    Object poster = freezed,
  }) {
    return _then(_MovieResponse(
      title: title == freezed ? _value.title : title as String,
      year: year == freezed ? _value.year : year as String,
      imdbId: imdbId == freezed ? _value.imdbId : imdbId as String,
      type: type == freezed ? _value.type : type as String,
      poster: poster == freezed ? _value.poster : poster as String,
    ));
  }
}

@JsonSerializable()
class _$_MovieResponse implements _MovieResponse {
  const _$_MovieResponse(
      {@JsonKey(name: 'Title') this.title,
      @JsonKey(name: 'Year') this.year,
      @JsonKey(name: 'imdbId') this.imdbId,
      @JsonKey(name: 'Type') this.type,
      @JsonKey(name: 'Poster') this.poster});

  factory _$_MovieResponse.fromJson(Map<String, dynamic> json) =>
      _$_$_MovieResponseFromJson(json);

  @override
  @JsonKey(name: 'Title')
  final String title;
  @override
  @JsonKey(name: 'Year')
  final String year;
  @override
  @JsonKey(name: 'imdbId')
  final String imdbId;
  @override
  @JsonKey(name: 'Type')
  final String type;
  @override
  @JsonKey(name: 'Poster')
  final String poster;

  @override
  String toString() {
    return 'MovieResponse(title: $title, year: $year, imdbId: $imdbId, type: $type, poster: $poster)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _MovieResponse &&
            (identical(other.title, title) ||
                const DeepCollectionEquality().equals(other.title, title)) &&
            (identical(other.year, year) ||
                const DeepCollectionEquality().equals(other.year, year)) &&
            (identical(other.imdbId, imdbId) ||
                const DeepCollectionEquality().equals(other.imdbId, imdbId)) &&
            (identical(other.type, type) ||
                const DeepCollectionEquality().equals(other.type, type)) &&
            (identical(other.poster, poster) ||
                const DeepCollectionEquality().equals(other.poster, poster)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(title) ^
      const DeepCollectionEquality().hash(year) ^
      const DeepCollectionEquality().hash(imdbId) ^
      const DeepCollectionEquality().hash(type) ^
      const DeepCollectionEquality().hash(poster);

  @override
  _$MovieResponseCopyWith<_MovieResponse> get copyWith =>
      __$MovieResponseCopyWithImpl<_MovieResponse>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$_$_MovieResponseToJson(this);
  }
}

abstract class _MovieResponse implements MovieResponse {
  const factory _MovieResponse(
      {@JsonKey(name: 'Title') String title,
      @JsonKey(name: 'Year') String year,
      @JsonKey(name: 'imdbId') String imdbId,
      @JsonKey(name: 'Type') String type,
      @JsonKey(name: 'Poster') String poster}) = _$_MovieResponse;

  factory _MovieResponse.fromJson(Map<String, dynamic> json) =
      _$_MovieResponse.fromJson;

  @override
  @JsonKey(name: 'Title')
  String get title;
  @override
  @JsonKey(name: 'Year')
  String get year;
  @override
  @JsonKey(name: 'imdbId')
  String get imdbId;
  @override
  @JsonKey(name: 'Type')
  String get type;
  @override
  @JsonKey(name: 'Poster')
  String get poster;
  @override
  _$MovieResponseCopyWith<_MovieResponse> get copyWith;
}
