import 'package:equatable/equatable.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:movie_recomendation/domain/entities/movie.dart';
import 'package:movie_recomendation/domain/entities/search_result.dart';

part 'movie_list_response.freezed.dart';
part 'movie_list_response.g.dart';

// ignore: must_be_immutable
@freezed
abstract class MovieListResponse with _$MovieListResponse{

  @JsonSerializable(explicitToJson: true)
  const factory MovieListResponse({
    @JsonKey(name: 'Search')
    List<MovieResponse> movies,

    @JsonKey(name: 'totalResults')
    String totalResults,

    @JsonKey(name: 'Response')
    String response
  }) = _MovieListResponse;

  factory MovieListResponse.fromJson(Map<dynamic, dynamic> json) =>
    _$MovieListResponseFromJson(json);


}

extension MovieListResponseMapper on MovieListResponse {
  SearchResult toEntity(){
    var listMovie = new List<Movie>();
    movies.forEach((e) {
      listMovie.add(e.toEntity());
    });

    return SearchResult(totalResult: totalResults, searchResult: listMovie);
  }
}

@freezed
abstract class MovieResponse with _$MovieResponse{

  @JsonSerializable()
  const factory MovieResponse({
    @JsonKey(name: 'Title')
    String title,
    @JsonKey(name: 'Year')
    String year,
    @JsonKey(name: 'imdbId')
    String imdbId,
    @JsonKey(name: 'Type')
    String type,
    @JsonKey(name: 'Poster')
    String poster
  }) = _MovieResponse;

  factory MovieResponse.fromJson(Map<dynamic, dynamic> json) =>
      _$MovieResponseFromJson(json);

}

extension MovieResponseMapper on MovieResponse {
  Movie toEntity() => Movie(
    title: title,
    year: year,
    type: type,
    poster: poster
  );
}
