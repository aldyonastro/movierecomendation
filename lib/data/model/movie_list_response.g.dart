// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'movie_list_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_MovieListResponse _$_$_MovieListResponseFromJson(Map<String, dynamic> json) {
  return _$_MovieListResponse(
    movies: (json['Search'] as List)
        ?.map((e) => e == null
            ? null
            : MovieResponse.fromJson(e as Map<String, dynamic>))
        ?.toList(),
    totalResults: json['totalResults'] as String,
    response: json['Response'] as String,
  );
}

Map<String, dynamic> _$_$_MovieListResponseToJson(
        _$_MovieListResponse instance) =>
    <String, dynamic>{
      'Search': instance.movies?.map((e) => e?.toJson())?.toList(),
      'totalResults': instance.totalResults,
      'Response': instance.response,
    };

_$_MovieResponse _$_$_MovieResponseFromJson(Map<String, dynamic> json) {
  return _$_MovieResponse(
    title: json['Title'] as String,
    year: json['Year'] as String,
    imdbId: json['imdbId'] as String,
    type: json['Type'] as String,
    poster: json['Poster'] as String,
  );
}

Map<String, dynamic> _$_$_MovieResponseToJson(_$_MovieResponse instance) =>
    <String, dynamic>{
      'Title': instance.title,
      'Year': instance.year,
      'imdbId': instance.imdbId,
      'Type': instance.type,
      'Poster': instance.poster,
    };
