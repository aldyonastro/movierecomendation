import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:logger/logger.dart';
import 'package:logging/logging.dart';
import 'package:flutter_flipperkit/flutter_flipperkit.dart';
import 'package:movie_recomendation/core/util/log/log.dart';
import 'package:movie_recomendation/presentation/movie_app.dart';
import 'package:movie_recomendation/presentation/movie_base_widget.dart';
import 'package:movie_recomendation/presentation/movie_bloc_delegate.dart';
import 'package:logger/logger.dart';
import 'core/di/service_locator.dart';
import 'data/db/app_database_migration_listener.dart';
import 'data/db/database_helper.dart';

enum EnvType {
  STAGING,
  PRODUCTION
}

class Env {

  static Env value;

  String appName;
  String baseUrl;
  Color primarySwatch;
  EnvType environmentType = EnvType.STAGING;

  // Database Config
  int dbVersion = 1;
  String dbName;

  Env() {
    value = this;
    init();
  }

  void init() async{
    WidgetsFlutterBinding.ensureInitialized();

    if(EnvType.STAGING == environmentType){
//      configureFlipper();
      configureLog();
    }

    Bloc.observer = MovieBlocDelegate();

//    AppDatabaseMigrationListener migrationListener = AppDatabaseMigrationListener();
//    DatabaseConfig databaseConfig = DatabaseConfig(Env.value.dbVersion, Env.value.dbName, migrationListener);
//    await configureServiceLocator(Environment.dev);

    var application = MovieRecommendApp();
    application.configureRoute();

    runApp(MovieBaseWidget(application));
  }

  void configureFlipper() {
//    FlipperClient flipperClient = FlipperClient.getDefault();
//    flipperClient.addPlugin(new FlipperNetworkPlugin(useHttpOverrides: false,));
//    flipperClient.addPlugin(new FlipperSharedPreferencesPlugin());
//    flipperClient.start();
  }

  void configureLog(){
//    var logger = Log();
//
//    switch(Env.value.environmentType){
//      case EnvType.STAGING:{
//        logger.setLevel(Logger.level);
//        break;
//      }
//      case EnvType.PRODUCTION:{
//        Logger.setLevel(Level.INFO);
//        break;
//      }
//    }
  }
}