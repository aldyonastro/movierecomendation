import 'package:flutter/cupertino.dart';
import 'package:injectable/injectable.dart';
import 'package:logger/logger.dart';

@injectable
class Log{
  Logger _instance;

  Log(){
    _instance = Logger(
      printer: PrefixPrinter(PrettyPrinter(
          methodCount: 2,
          errorMethodCount: 8,
          lineLength: 100,
          colors: false,
          printEmojis: false,
          printTime: true
      )),
    );
  }

  void setLevel(Level level){
    Logger.level = level;
  }

  void verbose(message, [Object error, StackTrace stackTrace]){
    _instance.v(message, error, stackTrace);
  }

  void debug(message, [Object error, StackTrace stackTrace]){
    _instance.d(message, error, stackTrace);
  }

  void info(message, [Object error, StackTrace stackTrace]){
    _instance.i(message, error, stackTrace);
  }

  void warning(message, [Object error, StackTrace stackTrace]){
    _instance.w(message, error, stackTrace);
  }

  void error(message, [Object error, StackTrace stackTrace]){
    _instance.e(message, error, stackTrace);
  }

  void wtf(message, [Object error, StackTrace stackTrace]){
    _instance.wtf(message, error, stackTrace);
  }
}
