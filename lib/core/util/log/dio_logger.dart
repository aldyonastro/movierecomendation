import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:movie_recomendation/core/util/log/log.dart';

@injectable
class DioLogger{

  DioLogger(this._log);

  Log _log;

  void onSend(String tag, RequestOptions options){
    _log.info('Request Path : [${options.method}] ${options.baseUrl}${options.path} \n'
        'Header : ${options.headers} \n '
        'Query param : ${options.queryParameters} \n ');
  }

  void onSuccess(String tag, Response response){
    _log.info('Response Path : [${response.request.method}] ${response.request.baseUrl}${response.request.path} \n'
              'Response statusCode : ${response.statusCode}');
    _log.info(response.data);
  }

  void onError(String tag, DioError error){
    if(null != error.response){
      _log.info('$tag - Error Path : [${error.response.request.method}] ${error.response.request.baseUrl}${error.response.request.path} Request Data : ${error.response.request.data.toString()}');
      _log.info('$tag - Error statusCode : ${error.response.statusCode}');
      _log.info('$tag - Error data : ${null != error.response.data ? error.response.data.toString() : ''}');
    }
    _log.info('$tag - Error Message : ${error.message}');
  }


}