import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:injectable/injectable.dart';

abstract class INetworkUtils {
  Future<bool> get isConnected;
}

@Injectable(as: INetworkUtils)
class NetworkInfo implements INetworkUtils {
  const NetworkInfo(this.connectionChecker);

  final DataConnectionChecker connectionChecker;

  @override
  Future<bool> get isConnected => connectionChecker.hasConnection;
}
