import 'package:dio/dio.dart';

class ServerException implements Exception {}

class CacheException implements Exception {}

class UnknownException implements Exception {}

class HttpException implements Exception{
  Response response;

  HttpException(this.response);
}

class MovieNotFoundException implements Exception {}