import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:fluro/fluro.dart';
import 'package:injectable/injectable.dart';
import 'package:path_provider/path_provider.dart';

@module
abstract class RegisterModule {

  @lazySingleton
  DataConnectionChecker get connectionChecker => DataConnectionChecker();

  @lazySingleton
  Router get router => Router();
}
