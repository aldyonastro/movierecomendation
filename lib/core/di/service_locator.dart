import 'package:fluro/fluro.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:movie_recomendation/data/db/database_helper.dart';
import 'package:movie_recomendation/data/network/api/api_provider.dart';
import 'package:movie_recomendation/data/repository/movie_repository.dart';

import 'service_locator.config.dart';

final GetIt sl = GetIt.instance;

@injectableInit
Future<void> configureServiceLocator(String env) async {
  await $initGetIt(sl, environment: env);
}