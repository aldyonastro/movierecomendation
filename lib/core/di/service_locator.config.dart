// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:fluro/fluro.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/get_it_helper.dart';

import '../../data/db/movie_db_repository.dart';
import '../../data/network/api/api_provider.dart';
import '../../data/repository/movie_repository.dart';
import '../../domain/get_movie_search_interactor.dart';
import '../../domain/repository/movie_repository.dart';
import '../../main_production.dart';
import '../../main_staging.dart';
import '../../presentation/screen/home/bloc/home_bloc.dart';
import '../util/log/dio_logger.dart';
import '../util/log/log.dart';
import '../util/networks/network_utils.dart';
import 'register_module.dart';

/// Environment names
const _prod = 'prod';
const _dev = 'dev';

/// adds generated dependencies
/// to the provided [GetIt] instance

void $initGetIt(GetIt g, {String environment}) {
  final gh = GetItHelper(g, environment);
  final registerModule = _$RegisterModule();
  gh.lazySingleton<DataConnectionChecker>(
      () => registerModule.connectionChecker);
  gh.factory<INetworkUtils>(() => NetworkInfo(g<DataConnectionChecker>()));
  gh.factory<Log>(() => Log());
  gh.lazySingleton<MovieDbRepository>(() => MovieDbRepository());
  gh.factory<Production>(() => Production(), registerFor: {_prod});
  gh.lazySingleton<Router>(() => registerModule.router);
  gh.factory<Staging>(() => Staging(), registerFor: {_dev});
  gh.factory<DioLogger>(() => DioLogger(g<Log>()));
  gh.lazySingleton<ApiProvider>(() => ApiProvider(g<DioLogger>()));
  gh.factory<IMovieRepository>(
      () => MovieApiRepository(g<INetworkUtils>(), g<ApiProvider>()));
  gh.lazySingleton<GetMovieSearchInteractor>(
      () => GetMovieSearchInteractor(g<IMovieRepository>()));
  gh.factory<HomeBloc>(() => HomeBloc(g<GetMovieSearchInteractor>(), g<Log>()));
}

class _$RegisterModule extends RegisterModule {}
