import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:movie_recomendation/env.dart';
import 'package:movie_recomendation/data/db/database_helper.dart';

import 'core/di/service_locator.dart';
import 'data/db/app_database_migration_listener.dart';

void main() => Staging();

@Environment("dev")
@injectable
class Staging extends Env {
  final String appName = "Movie Recommendation Staging";

  final String baseUrl = 'https://api.staging.website.org';
  final Color primarySwatch = Colors.amber;
  EnvType environmentType = EnvType.STAGING;

  final String dbName = 'movierec-stg.db';

  @override
  Future<void> init() async {
    await configureServiceLocator(Environment.dev);
    super.init();
  }
}
