import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:movie_recomendation/env.dart';

import 'core/di/service_locator.dart';

void main() => Production();

@Environment("prod")
@injectable
class Production extends Env {
  final String appName = "Movie Recommendation";

  final String baseUrl = 'https://api.website.org';
  final Color primarySwatch = Colors.teal;
  EnvType environmentType = EnvType.PRODUCTION;

  final String dbName = 'movierec.db';

  @override
  Future<void> init() async {
    await configureServiceLocator(Environment.prod);
    super.init();
  }
}